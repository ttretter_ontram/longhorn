package net.sf.okapi.lib.longhornapi.impl.rest.transport;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

public class XMLStepConfigOverrideListTest {

	@Test
	public void testStepConfigOverrides() throws Exception {
		// The first <e> contains no type, which is the legacy behavior. This should
		// default to being parsed as a StepConfigOverride
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
				" <l>\n" +
				" 	<e>\n" +
				" 		<stepClassName>abc</stepClassName>\n" +
				" 		<stepParams>def</stepParams>\n" +
				" 	</e>\n" +
				" 	<e type=\"filterConfig\">\n" +
				"       <fileExtension>.html</fileExtension>\n" +
				" 		<filterName>ghi</filterName>\n" +
				" 		<filterParams>jkl</filterParams>\n" +
				"   </e>\n" +
				" 	<e type=\"stepConfig\">\n" +
				" 		<stepClassName>mno</stepClassName>\n" +
				" 		<stepParams>pqr</stepParams>\n" +
				" 	</e>\n" +
				" 	<e type=\"filterConfig\">\n" +
				"       <fileExtension>.xml</fileExtension>\n" +
				" 		<filterName>okf_xmlstream-dita</filterName>\n" +
				" 	</e>\n" +
				"</l>";
		XMLStepConfigOverrideList list = XMLStepConfigOverrideList.unmarshal(xml);
		List<PipelineOverride> overrides = list.getElements();
		assertEquals(4, overrides.size());
		assertEquals(StepConfigOverride.class, overrides.get(0).getClass());
		assertEquals("abc", ((StepConfigOverride)overrides.get(0)).getStepClassName());
		assertEquals("def", ((StepConfigOverride)overrides.get(0)).getStepParams());
		assertEquals("stepConfig", ((StepConfigOverride)overrides.get(0)).getType());
		assertEquals(FilterConfigOverride.class, overrides.get(1).getClass());
		assertEquals(".html", ((FilterConfigOverride)overrides.get(1)).getFileExtension());
		assertEquals("ghi", ((FilterConfigOverride)overrides.get(1)).getFilterName());
		assertEquals("jkl", ((FilterConfigOverride)overrides.get(1)).getFilterParams());
		assertEquals("filterConfig", ((FilterConfigOverride)overrides.get(1)).getType());
		assertEquals(StepConfigOverride.class, overrides.get(2).getClass());
		assertEquals("mno", ((StepConfigOverride)overrides.get(2)).getStepClassName());
		assertEquals("pqr", ((StepConfigOverride)overrides.get(2)).getStepParams());
		assertEquals("stepConfig", ((StepConfigOverride)overrides.get(2)).getType());
		assertEquals("filterConfig", ((FilterConfigOverride)overrides.get(3)).getType());
		assertEquals(FilterConfigOverride.class, overrides.get(3).getClass());
		assertEquals(".xml", ((FilterConfigOverride)overrides.get(3)).getFileExtension());
		assertEquals("okf_xmlstream-dita", ((FilterConfigOverride)overrides.get(3)).getFilterName());
		assertNull(((FilterConfigOverride)overrides.get(3)).getFilterParams());
	}

	@Test
	public void serialize() throws Exception {
		String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + 
				"<l>" +
				"<e type=\"stepConfig\">" +
				"<stepClassName>abc</stepClassName>" +
				"<stepParams>def</stepParams>" +
				"</e>" +
				"<e type=\"filterConfig\">" +
				"<fileExtension>.html</fileExtension>" +
				"<filterName>ghi</filterName>" +
				"<filterParams>jkl</filterParams>" +
				"</e>" +
				"<e type=\"filterConfig\">" +
				"<fileExtension>.xml</fileExtension>" +
				"<filterName>okf_xmlstream-dita</filterName>" +
				"</e>" +
				"</l>";

		XMLStepConfigOverrideList list = new XMLStepConfigOverrideList();
		StepConfigOverride step = new StepConfigOverride();
		step.setStepClassName("abc");
		step.setStepParams("def");
		list.add(step);
		FilterConfigOverride filter = new FilterConfigOverride();
		filter.setFileExtension(".html");
		filter.setFilterName("ghi");
		filter.setFilterParams("jkl");
		list.add(filter);
		filter = new FilterConfigOverride();
		filter.setFileExtension(".xml");
		filter.setFilterName("okf_xmlstream-dita");
		list.add(filter);
		String xml = XMLStepConfigOverrideList.marshal(list);
		assertEquals(expected, xml);
	}
}
